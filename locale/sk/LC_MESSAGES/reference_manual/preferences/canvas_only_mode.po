# translation of docs_krita_org_reference_manual___preferences___canvas_only_mode.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___preferences___canvas_only_mode\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-04-02 12:45+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/preferences/canvas_only_mode.rst:1
msgid "Canvas only mode settings in Krita."
msgstr ""

#: ../../reference_manual/preferences/canvas_only_mode.rst:11
#: ../../reference_manual/preferences/canvas_only_mode.rst:16
msgid "Canvas Only Mode"
msgstr "Iba režim plátna"

#: ../../reference_manual/preferences/canvas_only_mode.rst:11
msgid "Preferences"
msgstr ""

#: ../../reference_manual/preferences/canvas_only_mode.rst:11
msgid "Settings"
msgstr ""

#: ../../reference_manual/preferences/canvas_only_mode.rst:18
msgid ""
"Canvas Only mode is Krita's version of full screen mode. It is activated by "
"hitting the :kbd:`Tab` key on the keyboard. Select which parts of Krita will "
"be hidden in canvas-only mode -- The user can set which UI items will be "
"hidden in canvas-only mode. Selected items will be hidden."
msgstr ""
