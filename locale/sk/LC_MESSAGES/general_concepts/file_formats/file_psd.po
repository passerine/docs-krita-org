# translation of docs_krita_org_general_concepts___file_formats___file_psd.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_general_concepts___file_formats___file_psdm\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-03-01 14:12+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../general_concepts/file_formats/file_psd.rst:1
msgid "The Photoshop file format as exported by Krita."
msgstr "Súborový formát Photoshop ako exportovaný z Krita."

#: ../../general_concepts/file_formats/file_psd.rst:10
#, fuzzy
#| msgid "\\*.psd"
msgid "*.psd"
msgstr "\\*.psd"

#: ../../general_concepts/file_formats/file_psd.rst:10
msgid "PSD"
msgstr ""

#: ../../general_concepts/file_formats/file_psd.rst:10
msgid "Photoshop Document"
msgstr ""

#: ../../general_concepts/file_formats/file_psd.rst:15
msgid "\\*.psd"
msgstr "\\*.psd"

#: ../../general_concepts/file_formats/file_psd.rst:17
msgid ""
"``.psd`` is Photoshop's internal file format. For some reason, people like "
"to use it as an interchange format, even though it is not designed for this."
msgstr ""

#: ../../general_concepts/file_formats/file_psd.rst:19
msgid ""
"``.psd``, unlike actual interchange formats like :ref:`file_pdf`, :ref:"
"`file_tif`, :ref:`file_exr`, :ref:`file_ora` and :ref:`file_svg` doesn't "
"have an official spec online. Which means that it needs to be reverse "
"engineered. Furthermore, as an internal file format, it doesn't have much of "
"a philosophy to its structure, as it's only purpose is to save what "
"Photoshop is busy with, or rather, what all the past versions of Photoshop "
"have been busy with. This means that the inside of a PSD looks somewhat like "
"Photoshop's virtual brains, and PSD is in general a very disliked file-"
"format."
msgstr ""

#: ../../general_concepts/file_formats/file_psd.rst:21
msgid ""
"Due to ``.psd`` being used as an interchange format, this leads to confusion "
"amongst people using these programs, as to why not all programs support "
"opening these. Sometimes, you might even see users saying that a certain "
"program is terrible because it doesn't support opening PSDs properly. But as "
"PSD is an internal file-format without online specs, it is impossible to "
"have any program outside it support it 100%."
msgstr ""

#: ../../general_concepts/file_formats/file_psd.rst:23
msgid ""
"Krita supports loading and saving raster layers, blending modes, "
"layerstyles, layer groups, and transparency masks from PSD. It will likely "
"never support vector and text layers, as these are just too difficult to "
"program properly."
msgstr ""

#: ../../general_concepts/file_formats/file_psd.rst:25
msgid ""
"We recommend using any other file format instead of PSD if possible, with a "
"strong preference towards :ref:`file_ora` or :ref:`file_tif`."
msgstr ""

#: ../../general_concepts/file_formats/file_psd.rst:27
msgid ""
"As a working file format, PSDs can be expected to become very heavy and most "
"websites won't accept them."
msgstr ""
