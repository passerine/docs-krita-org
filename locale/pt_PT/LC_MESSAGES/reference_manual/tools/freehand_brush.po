# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 10:23+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: paintingwithassistants assistanttool icons Krita\n"
"X-POFile-SpellExtra: demão image menuselection kbd freehandbrushtool\n"
"X-POFile-SpellExtra: Graphire toolfreehandbrush Wacom guilabel images alt\n"
"X-POFile-SpellExtra: ref\n"

#: ../../<rst_epilog>:22
msgid ""
".. image:: images/icons/freehand_brush_tool.svg\n"
"   :alt: toolfreehandbrush"
msgstr ""
".. image:: images/icons/freehand_brush_tool.svg\n"
"   :alt: ferramenta de pincel à mão"

#: ../../reference_manual/tools/freehand_brush.rst:1
msgid ""
"Krita's freehand brush tool reference, containing how to use the stabilizer "
"in krita."
msgstr ""
"A referência da ferramenta de pincel de desenho à mão do Krita, incluindo "
"como usar o estabilizador no Krita."

#: ../../reference_manual/tools/freehand_brush.rst:12
msgid "Freehand Brush"
msgstr "Pincel de Traço Livre à Mão"

#: ../../reference_manual/tools/freehand_brush.rst:12
msgid "Freehand"
msgstr "Caminho Livre à Mão"

#: ../../reference_manual/tools/freehand_brush.rst:17
msgid "Freehand Brush Tool"
msgstr "Ferramenta do Pincel de Traço Livre à Mão"

#: ../../reference_manual/tools/freehand_brush.rst:19
msgid "|toolfreehandbrush|"
msgstr "|toolfreehandbrush|"

#: ../../reference_manual/tools/freehand_brush.rst:21
msgid ""
"The default tool you have selected on Krita start-up, and likely the tool "
"that you will use the most."
msgstr ""
"A ferramenta predefinida que está seleccionada no arranque do Krita, e "
"provavelmente a ferramenta que será mais usada."

#: ../../reference_manual/tools/freehand_brush.rst:23
msgid ""
"The freehand brush tool allows you to paint on paint layers without "
"constraints like the straight line tool. It makes optimal use of your "
"tablet's input settings to control the brush-appearance. To switch the "
"brush, make use of the brush-preset docker."
msgstr ""
"A ferramenta de desenho à mão permite-lhe pintar em camadas de pintura sem "
"quaisquer restrições, como acontece na linha recta. Tira o máximo de partido "
"dos dados gerados pela sua tablete para controlar a aparência do pincel. "
"Para mudar de pincel, tire partido da área de predefinições do pincel."

#: ../../reference_manual/tools/freehand_brush.rst:27
msgid "Hotkeys and Sticky keys"
msgstr "Atalhos e Teclas Fixas"

#: ../../reference_manual/tools/freehand_brush.rst:29
msgid "The freehand brush tool's hotkey is :kbd:`B`."
msgstr "A combinação de teclas da ferramenta de pincel à mão é o :kbd:`B`."

#: ../../reference_manual/tools/freehand_brush.rst:31
msgid ""
"The alternate invocation is the ''color picker'' (standardly invoked by the :"
"kbd:`Ctrl` key). Press the :kbd:`Ctrl` key to switch the tool to \"color "
"picker\", use left or right click to pick fore and background color "
"respectively. Release the :kbd:`Ctrl` key to return to the freehand brush "
"tool."
msgstr ""
"A invocação alternativa será com o ''selector de cores'' (normalmente "
"invocado com o :kbd:`Ctrl`) Carregue em :kbd:`Ctrl` para mudar a ferramenta "
"para o \"selector de cores\"; use o botão esquerdo ou direito do rato para "
"escolher a cor principal e a de fundo, respectivamente. Largue a tecla :kbd:"
"`Ctrl` para voltar à ferramenta de desenho à mão."

#: ../../reference_manual/tools/freehand_brush.rst:32
msgid ""
"The Primary setting is \"size\" (standardly invoked by the :kbd:`Shift` "
"key). Press the :kbd:`Shift` key and drag outward to increase brush size. "
"Drag inward to decrease it."
msgstr ""
"A configuração principal é o \"tamanho\" (que pode ser invocada normalmente "
"com o :kbd:`Shift`). Se carregar com o :kbd:`Shift` e arrastar para fora, "
"irá aumentar o tamanho do pincel. Arraste para dentro para o diminuir."

#: ../../reference_manual/tools/freehand_brush.rst:33
msgid ""
"You can also press the :kbd:`V` key as a stickykey for the straight-line "
"tool."
msgstr ""
"Também poderá carregar o :kbd:`V` com tecla fixa para a ferramenta da linha "
"recta."

#: ../../reference_manual/tools/freehand_brush.rst:35
msgid ""
"The hotkey can be edited in :menuselection:`Settings --> Configure Krita --> "
"Configure Shortcuts`. The sticky-keys can be edited in :menuselection:"
"`Settings --> Configure Krita --> Canvas Input Settings`."
msgstr ""
"A combinação de teclas pode ser alterada em :menuselection:`Configuração --> "
"Configurar o Krita --> Configurar os Atalhos`. As teclas fixas podem ser "
"editadas em :menuselection:`Configuração --> Configurar o Krita --> "
"Introdução na Área de Desenho com a Tablete`."

#: ../../reference_manual/tools/freehand_brush.rst:39
msgid "Tool Options"
msgstr "Opções da Ferramenta"

#: ../../reference_manual/tools/freehand_brush.rst:41
#: ../../reference_manual/tools/freehand_brush.rst:79
msgid "Stabilizer"
msgstr "Estabilizador"

#: ../../reference_manual/tools/freehand_brush.rst:41
msgid "Basic Smooth"
msgstr "Suavização Básica"

#: ../../reference_manual/tools/freehand_brush.rst:41
msgid "No Smoothing"
msgstr "Sem Suavização"

#: ../../reference_manual/tools/freehand_brush.rst:41
msgid "Weighted Smoothing"
msgstr "Suavização Ponderada"

#: ../../reference_manual/tools/freehand_brush.rst:45
msgid "Smoothing"
msgstr "Suavização"

#: ../../reference_manual/tools/freehand_brush.rst:47
msgid ""
"Smoothing, also known as stabilising in some programs, allows the program to "
"correct the stroke. Useful for people with shaky hands, or particularly "
"difficult long lines."
msgstr ""
"A suavização, também conhecida por estabilização em alguns programas, "
"permite ao programa corrigir o traço. É útil para pessoas com mãos trémulas "
"ou com linhas longa e, por isso, especialmente difíceis de desenhar."

#: ../../reference_manual/tools/freehand_brush.rst:49
msgid "The following options can be selected:"
msgstr "Poderá seleccionar as seguintes opções:"

#: ../../reference_manual/tools/freehand_brush.rst:51
msgid "No Smoothing."
msgstr "Sem Suavização."

#: ../../reference_manual/tools/freehand_brush.rst:52
msgid ""
"The input from the tablet translates directly to the screen. This is the "
"fastest option, and good for fine details."
msgstr ""
"A geração de dados da tablete traduz-se directamente no ecrã. Esta é a opção "
"mais rápida, e é boa para pequenos detalhes."

#: ../../reference_manual/tools/freehand_brush.rst:53
msgid "Basic Smoothing."
msgstr "Suavização Básica."

#: ../../reference_manual/tools/freehand_brush.rst:54
msgid ""
"This option will smooth the input of older tablets like the Wacom Graphire "
"3. If you experience slightly jagged lines without any smoothing on, this "
"option will apply a very little bit of smoothing to get rid of those lines."
msgstr ""
"Esta opção irá suavizar os dados introduzidos pelas tabletes mais antigas, "
"como a Wacom Graphire 3. Se detectar linhas ligeiramente tremidas sem "
"qualquer suavização activada, esta opção irá aplicar uma suavização muito "
"ligeira para se ver livre dessas linhas."

#: ../../reference_manual/tools/freehand_brush.rst:56
msgid ""
"This option allows you to use the following parameters to make the smoothing "
"stronger or weaker:"
msgstr ""
"Esta opção permite-lhe usar os seguintes parâmetros para tornar a suavização "
"mais forte ou mais fraca:"

#: ../../reference_manual/tools/freehand_brush.rst:58
#: ../../reference_manual/tools/freehand_brush.rst:70
msgid "Distance"
msgstr "Distância"

#: ../../reference_manual/tools/freehand_brush.rst:59
msgid ""
"The distance the brush needs to move before the first dab is drawn. "
"(Literally the amount of events received by the tablet before the first dab "
"is drawn.)"
msgstr ""
"A distância que o pincel precisa de se mover antes de ser desenhada a "
"primeira demão. (Literalmente o número de eventos recebidos pela tablete "
"antes de desenhar a primeira demão.)"

#: ../../reference_manual/tools/freehand_brush.rst:60
msgid "Stroke Ending"
msgstr "Finalização dos Traços"

#: ../../reference_manual/tools/freehand_brush.rst:61
msgid ""
"This controls how much the line will attempt to reach the last known "
"position of the cursor after the left-mouse button/or stylus is lifted. Will "
"currently always result in a straight line, so use with caution."
msgstr ""
"Isto controla quanto é que a linha irá tentar atingir a última posição do "
"cursor, após ter levantado o botão esquerdo do rato ou o lápis. Irá resultar "
"sempre numa linha recta, por isso use com cuidado."

#: ../../reference_manual/tools/freehand_brush.rst:62
msgid "Smooth Pressure"
msgstr "Pressão Suave"

#: ../../reference_manual/tools/freehand_brush.rst:63
msgid ""
"This will apply the smoothing on the pressure input as well, resulting in "
"more averaged size for example."
msgstr ""
"Isto irá aplicar a suavização dos dados de pressão gerados, resultando num "
"tamanho mais próximo da média, por exemplo."

#: ../../reference_manual/tools/freehand_brush.rst:65
msgid "Weighted smoothing:"
msgstr "Suavização ponderada:"

#: ../../reference_manual/tools/freehand_brush.rst:65
#: ../../reference_manual/tools/freehand_brush.rst:79
msgid "Scalable Distance"
msgstr "Distância Escalável"

#: ../../reference_manual/tools/freehand_brush.rst:65
#: ../../reference_manual/tools/freehand_brush.rst:79
msgid ""
"This makes it so that the numbers involved will be scaled along the zoom "
"level."
msgstr ""
"Isto faz com que os números envolvidos vão sofrendo uma mudança de escala ao "
"longo do nível de ampliação."

#: ../../reference_manual/tools/freehand_brush.rst:68
msgid ""
"This option averages all inputs from the tablet. It is different from "
"weighted smoothing in that it allows for always completing the line. It will "
"draw a circle around your cursor and the line will be a bit behind your "
"cursor while painting."
msgstr ""
"Esta opção calcula a média de todos os parâmetros de entrada da tablete. É "
"diferente da suavização ponderada, na medida em que permite sempre finalizar "
"a linha. Irá desenhar um círculo em torno do seu cursor e a linha ficará um "
"pouco atrás do seu cursor enquanto pintar."

#: ../../reference_manual/tools/freehand_brush.rst:71
msgid "This is the strength of the smoothing."
msgstr "Esta é a potência da suavização."

#: ../../reference_manual/tools/freehand_brush.rst:72
msgid "Delay"
msgstr "Atraso"

#: ../../reference_manual/tools/freehand_brush.rst:73
msgid ""
"This toggles and determines the size of the dead zone around the cursor. "
"This can be used to create sharp corners with more control."
msgstr ""
"Isto activa/desactiva o tamanho das \"zonas mortas\" em torno do cursor. "
"Isto pode ser usado para criar cantos vincados com mais controlo."

#: ../../reference_manual/tools/freehand_brush.rst:74
msgid "Finish Line"
msgstr "Terminar a Linha"

#: ../../reference_manual/tools/freehand_brush.rst:75
msgid "This ensures that the line will be finished."
msgstr "Isto garante que a linha será finalizada."

#: ../../reference_manual/tools/freehand_brush.rst:76
msgid "Stabilize sensors"
msgstr "Estabilizar os sensores"

#: ../../reference_manual/tools/freehand_brush.rst:77
msgid ""
"Similar to :guilabel:`Smooth Pressure`, this allows the input (pressure, "
"speed, tilt) to be smoother."
msgstr ""
"Semelhante a :guilabel:`Suavizar a Pressão`, isto permite que os dados "
"gerados à entrada (pressão, velocidade, desvio) sejam mais suaves."

#: ../../reference_manual/tools/freehand_brush.rst:81
msgid "Painting Assistants"
msgstr "Assistentes de Pintura"

#: ../../reference_manual/tools/freehand_brush.rst:84
msgid "Assistants"
msgstr "Assistentes"

#: ../../reference_manual/tools/freehand_brush.rst:86
msgid ""
"Ticking this will allow snapping to :ref:`assistant_tool`, and the hotkey to "
"toggle it is :kbd:`Ctrl + Shift + L`. See :ref:`painting_with_assistants` "
"for more information."
msgstr ""
"Se assinalar isto, permitirá ajustar à :ref:`assistant_tool`, sendo a "
"combinação de teclas para a activar/desactivar o :kbd:`Ctrl + Shift + L`. "
"Veja mais informações em :ref:`painting_with_assistants`."

#: ../../reference_manual/tools/freehand_brush.rst:88
msgid ""
"The slider will determine the amount of snapping, with 1000 being perfect "
"snapping, and 0 being no snapping at all. For situations where there is more "
"than one assistant on the canvas, the defaultly ticked :guilabel:`Snap "
"Single` means that Krita will only snap to a single assistant at a time, "
"preventing noise. Unticking it allows you to chain assistants together and "
"snap along them."
msgstr ""
"A barra deslizante irá definir a quantidade de ajuste, sendo o 1000 um "
"perfeito ajuste, enquanto o 0 será a ausência total de ajuste. Para as "
"situações em que exista mais que um assistente na área de desenho, a opção "
"assinalada por omissão :guilabel:`Ajuste único` significa que o Krita só se "
"irá ajustar a um único assistente de cada vez, evitando o ruído. Se a "
"desligar, poderá encadear assistentes entre si e ajustar com base neles."
