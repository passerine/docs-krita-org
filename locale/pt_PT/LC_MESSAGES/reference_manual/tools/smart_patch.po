msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-17 16:49+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: en icons Krita image patch gif smartpatchtool images\n"
"X-POFile-SpellExtra: alt Smart toolsmartpatch tools\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../<rst_epilog>:62
msgid ""
".. image:: images/icons/smart_patch_tool.svg\n"
"   :alt: toolsmartpatch"
msgstr ""
".. image:: images/icons/smart_patch_tool.svg\n"
"   :alt: padrão inteligente"

#: ../../reference_manual/tools/smart_patch.rst:1
msgid "Krita's smart patch tool reference."
msgstr "A referência da ferramenta de padrões inteligentes do Krita."

#: ../../reference_manual/tools/smart_patch.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/smart_patch.rst:11
msgid "Smart Patch"
msgstr "Padrões Inteligentes"

#: ../../reference_manual/tools/smart_patch.rst:11
msgid "Automatic Healing"
msgstr "Reparação Automática"

#: ../../reference_manual/tools/smart_patch.rst:16
msgid "Smart Patch Tool"
msgstr "Ferramenta Inteligente de Padrões"

#: ../../reference_manual/tools/smart_patch.rst:18
msgid "|toolsmartpatch|"
msgstr "|toolsmartpatch|"

#: ../../reference_manual/tools/smart_patch.rst:20
msgid ""
"The smart patch tool allows you to seamlessly remove elements from the "
"image. It does this by letting you draw the area which has the element you "
"wish to remove, and then it will attempt to use patterns already existing in "
"the image to fill the blank."
msgstr ""
"A ferramenta de padrões inteligentes permite-lhe remover de forma "
"transparente alguns elementos da imagem. Ele faz isso da seguinte forma: "
"permite-lhe desenhar a área que tem o elemento que deseja remover e depois "
"tenta usar padrões já existentes na imagem para preencher o espaço vazio."

#: ../../reference_manual/tools/smart_patch.rst:22
msgid "You can see it as a smarter version of the clone brush."
msgstr "Poderá ver isto como uma forma mais inteligente do pincel de clonagem."

#: ../../reference_manual/tools/smart_patch.rst:25
msgid ".. image:: images/tools/Smart-patch.gif"
msgstr ".. image:: images/tools/Smart-patch.gif"

#: ../../reference_manual/tools/smart_patch.rst:26
msgid "The smart patch tool has the following tool options:"
msgstr ""
"A ferramenta de padrões inteligentes tem as seguintes opções da ferramenta:"

#: ../../reference_manual/tools/smart_patch.rst:29
msgid "Accuracy"
msgstr "Precisão"

#: ../../reference_manual/tools/smart_patch.rst:31
msgid ""
"Accuracy indicates how many samples, and thus how often the algorithm is "
"run. A low accuracy will do few samples, but will also run the algorithm "
"fewer times, making it faster. Higher accuracy will do many samples, making "
"the algorithm run more often and give more precise results, but because it "
"has to do more work, it is slower."
msgstr ""
"A precisão define quantas amostras e a forma como o algoritmo é executado. "
"Uma precisão baixa irá aplicar poucas amostras, mas também irá tentar "
"executar o algoritmo menos vezes, o que o tornará mais rápido. Uma maior "
"precisão irá fazer muitas amostras, executando o algoritmo com maior "
"frequência e dando resultados mais precisos; porém, como tem de trabalhar "
"mais, é deste modo mais lento."

#: ../../reference_manual/tools/smart_patch.rst:34
msgid "Patch size"
msgstr "Tamanho do padrão"

#: ../../reference_manual/tools/smart_patch.rst:36
msgid ""
"Patch size determines how big the size of the pattern to choose is. This "
"will be best explained with some testing, but if the surrounding image has "
"mostly small elements, like branches, a small patch size will give better "
"results, while a big patch size will be better for images with big elements, "
"so they get reused as a whole."
msgstr ""
"O tamanho do padrão define quão grande será o tamanho do padrão a escolher. "
"Isto será melhor explicado com alguns testes, mas se a imagem envolvente "
"tiver na sua maioria elementos pequenos, como ramos, um tamanho de padrão "
"mais reduzido irá gerar melhores resultados, enquanto um tamanho grande será "
"melhor para as imagens com elementos grandes, para que possam ser "
"reutilizados como um todo."
