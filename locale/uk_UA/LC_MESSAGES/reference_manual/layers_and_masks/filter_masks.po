# Translation of docs_krita_org_reference_manual___layers_and_masks___filter_masks.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___layers_and_masks___filter_masks\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 16:24+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/layers_and_masks/filter_masks.rst:None
msgid ".. image:: images/Krita_ghostlady_2.png"
msgstr ".. image:: images/Krita_ghostlady_2.png"

#: ../../reference_manual/layers_and_masks/filter_masks.rst:None
msgid ".. image:: images/Krita_ghostlady_3.png"
msgstr ".. image:: images/Krita_ghostlady_3.png"

#: ../../reference_manual/layers_and_masks/filter_masks.rst:1
msgid "How to use filter masks in Krita."
msgstr "Як користуватися масками фільтрування у Krita."

#: ../../reference_manual/layers_and_masks/filter_masks.rst:11
msgid "Layers"
msgstr "Шари"

#: ../../reference_manual/layers_and_masks/filter_masks.rst:11
msgid "Masks"
msgstr "Маски"

#: ../../reference_manual/layers_and_masks/filter_masks.rst:11
msgid "Filters"
msgstr "Фільтри"

#: ../../reference_manual/layers_and_masks/filter_masks.rst:16
msgid "Filter Masks"
msgstr "Маски фільтрування"

#: ../../reference_manual/layers_and_masks/filter_masks.rst:18
msgid ""
"Filter masks show an area of their layer with a filter (such as blur, "
"levels, brightness / contrast etc.). For example, if you select an area of a "
"paint layer and add a Filter Layer, you will be asked to choose a filter. If "
"you choose the blur filter, you will see the area you selected blurred."
msgstr ""
"Застосування масок фільтрування призводить до показу області їхнього впливу "
"на шарі з певним фільтром (зокрема розмиття, рівнів, яскравості або "
"контрастності тощо). Наприклад, якщо ви позначите ділянки шару малювання і "
"додасте шар фільтрування, програма попросить вас вибрати фільтр. Якщо ви "
"виберете фільтр розмивання, ви побачите, що зображення позначеної вами "
"ділянки стало розмитим."

#: ../../reference_manual/layers_and_masks/filter_masks.rst:24
msgid ""
"With filter masks, we can for example make this ghost-lady more ethereal by "
"putting a clone layer underneath, and setting a lens-blur filter on it."
msgstr ""
"За допомогою масок фільтрування ви, наприклад, можете зробити цю примарну "
"леді ще більш неземною, додавши під основним шаром шар-клон і застосувавши "
"до нього фільтр розмивання об'єктивом."

#: ../../reference_manual/layers_and_masks/filter_masks.rst:30
msgid ""
"Set the blending mode of the clone layer to :guilabel:`Color Dodge` and she "
"becomes really spooky!"
msgstr ""
"Встановіть режим змішування для шару клонування у значення :guilabel:"
"`Висвітлення кольорів` і ваша леді стане справжнім привидом!"

#: ../../reference_manual/layers_and_masks/filter_masks.rst:32
msgid ""
"Unlike applying a filter to a section of a paint layer directly, filter "
"masks do not permanently alter the original image. This means you can tweak "
"the filter (or the area it applies to) at any time. Changes can always be "
"altered or removed."
msgstr ""
"На відміну від застосування фільтра до частини шару малювання безпосередньо, "
"маски фільтрування не вносять змін до початкового зображення. Це означає, що "
"ви можете коли завгодно коригувати параметри фільтрування (або ділянку, до "
"якої застосовується фільтрування). Зміни також можна будь-коли редагувати "
"або вилучати."

#: ../../reference_manual/layers_and_masks/filter_masks.rst:34
msgid ""
"Unlike filter layers, filter masks apply only to the area you have selected "
"(the mask)."
msgstr ""
"На відміну від шарів фільтрування, маски фільтрування застосовуються лише до "
"позначеної вами ділянки (маски)."

#: ../../reference_manual/layers_and_masks/filter_masks.rst:36
msgid ""
"You can edit the settings for a filter mask at any time by double clicking "
"on it in the Layers docker. You can also change the selection that the "
"filter mask affects by selecting the filter mask in the Layers docker and "
"then using the paint tools in the main window. Painting white includes the "
"area, painting black excludes it, and all other colors are turned into a "
"shade of gray which applies proportionally."
msgstr ""
"Ви можете будь-коли редагувати параметри маски фільтрування. Для цього "
"достатньо двічі клацнути лівою кнопкою миші на пункті маски на бічній панелі "
"шарів. Ви також можете змінити позначену ділянку, до якої буде застосовано "
"маску фільтрування: позначте пункт маски фільтрування на бічній панелі шарів "
"і скористайтеся засобами малювання у головному вікні програми. Ділянки, які "
"буде намальовано білим кольором, буде включено до фільтрування, а чорним — "
"виключено. Усі проміжні кольори буде перетворено на відтінки сірого. Фільтр "
"для ділянок проміжних кольорів застосовуватиметься пропорційно до близькості "
"до білого кольору."
