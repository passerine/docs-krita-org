# Translation of docs_krita_org_tutorials___krita-brush-tips.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: tutorials\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-24 13:42+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: ../../tutorials/krita-brush-tips.rst:1
#: ../../tutorials/krita-brush-tips.rst:13
msgid ""
"Krita Brush-tips is an archive of brush-modification tutorials done by the "
"krita-foundation.tumblr.com account based on user requests."
msgstr ""
"Les puntes de pinzell del Krita són un arxiu de les guies d'aprenentatge per "
"a la modificació de pinzells realitzades en el compte de la krita-foundation."
"tumblr.com basades en les sol·licituds dels usuaris."

#: ../../tutorials/krita-brush-tips.rst:15
msgid "Topics:"
msgstr "Temes:"
