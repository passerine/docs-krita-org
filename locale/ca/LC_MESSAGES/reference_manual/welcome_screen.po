# Translation of docs_krita_org_reference_manual___welcome_screen.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 21:19+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../reference_manual/welcome_screen.rst:None
msgid ".. image:: images/welcome_screen.png"
msgstr ".. image:: images/welcome_screen.png"

#: ../../reference_manual/welcome_screen.rst:1
msgid "The welcome screen in Krita."
msgstr "La pantalla de benvinguda al Krita."

#: ../../reference_manual/welcome_screen.rst:10
#: ../../reference_manual/welcome_screen.rst:14
msgid "Welcome Screen"
msgstr "Pantalla de benvinguda"

#: ../../reference_manual/welcome_screen.rst:16
msgid ""
"When you open Krita, starting from version 4.1.3, you will be greeted by a "
"welcome screen. This screen makes it easy for you to get started with Krita, "
"as it provides a collection of shortcuts for the most common tasks that you "
"will probably be doing when you open Krita."
msgstr ""
"Quan obriu el Krita, a partir de la versió 4.1.3, apareixerà una pantalla de "
"benvinguda. Aquesta pantalla facilita començar amb el Krita, ja que "
"proporciona una col·lecció de dreceres per a les tasques més habituals que "
"probablement realitzareu en obrir el Krita."

#: ../../reference_manual/welcome_screen.rst:23
msgid "The screen is divided into 4 sections:"
msgstr "La pantalla es divideix en 4 seccions:"

#: ../../reference_manual/welcome_screen.rst:25
msgid ""
"The :guilabel:`Start` section there are links to create new document as well "
"to open an existing document."
msgstr ""
"A la secció :guilabel:`Inici` hi ha enllaços per a crear un document nou i "
"per obrir un document existent."

#: ../../reference_manual/welcome_screen.rst:28
msgid ""
"The :guilabel:`Recent Documents` section has a list of recently opened "
"documents from your previous sessions in Krita."
msgstr ""
"A la secció :guilabel:`Documents recents` hi ha una llista dels documents "
"oberts recentment en les vostres sessions anteriors en el Krita."

#: ../../reference_manual/welcome_screen.rst:31
msgid ""
"The :guilabel:`Community` section provides some links to get help, "
"Supporting development of Krita, Source code of Krita and to links to "
"interact with our user community."
msgstr ""
"A la secció :guilabel:`Comunitat` es proporcionen alguns enllaços per "
"obtenir ajuda, donar recolzament al desenvolupament del Krita, el codi font "
"del Krita i enllaços per interactuar amb la nostra comunitat d'usuaris."

#: ../../reference_manual/welcome_screen.rst:35
msgid ""
"The :guilabel:`News` section, which is disabled by default, when enabled "
"provides you with latest news feeds fetched from Krita website, this will "
"help you stay up to date with the release news and other events happening in "
"our community."
msgstr ""
"A la secció :guilabel:`Notícies`, la qual de manera predeterminada està "
"inhabilitada, quan està habilitada proporciona les últimes notícies que es "
"troben al lloc web del Krita, el qual us ajudarà a mantenir-vos al dia amb "
"les notícies de llançament i altres esdeveniments que ocorren en la nostra "
"comunitat."

#: ../../reference_manual/welcome_screen.rst:39
msgid ""
"Other than the above sections the welcome screen also acts as a drop area "
"for opening any document. You just have to drag and drop a Krita document or "
"any supported image files on the empty area around the sections to open it "
"in Krita."
msgstr ""
"A part de les seccions anteriors, la pantalla de benvinguda també actua com "
"una àrea per obrir qualsevol document. Només haureu d'arrossegar i deixar "
"anar un document al Krita o qualsevol fitxer d'imatge compatible a l'àrea "
"buida al voltant de les seccions per obrir-lo en el Krita."
