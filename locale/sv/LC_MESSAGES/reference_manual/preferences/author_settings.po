# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:28+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: vänsterknapp"

#: ../../reference_manual/preferences/author_settings.rst:1
msgid "Author profile settings in Krita."
msgstr "Inställningar av upphovsmannaprofil i Krita."

#: ../../reference_manual/preferences/author_settings.rst:11
msgid "Preferences"
msgstr "Anpassning"

#: ../../reference_manual/preferences/author_settings.rst:11
msgid "Settings"
msgstr "Inställningar"

#: ../../reference_manual/preferences/author_settings.rst:11
msgid "Author Profile"
msgstr "Upphovsmannaprofil"

#: ../../reference_manual/preferences/author_settings.rst:11
msgid "Metadata"
msgstr "Metadata"

#: ../../reference_manual/preferences/author_settings.rst:16
msgid "Author Profile Settings"
msgstr "Inställningar av upphovsmannaprofil"

#: ../../reference_manual/preferences/author_settings.rst:18
msgid ""
"Krita allows creating an author profile that you can use to store contact "
"info into your images."
msgstr ""
"Krita tillåter att en upphovsmannaprofil skapas där man kan lagra "
"kontaktinformation i sina bilder."

#: ../../reference_manual/preferences/author_settings.rst:20
msgid ""
"The main element is the author page. This page was overhauled massively in "
"4.0."
msgstr ""
"Huvudelementet är upphovsmannasidan. Sidan har fått en omfattande översyn i "
"4.0."

#: ../../reference_manual/preferences/author_settings.rst:22
msgid ""
"By default, it will use the \"Anonymous\" profile, which contains nothing. "
"To create a new profile, press the \"+\" button, and write up a name for the "
"author profile."
msgstr ""
"Normalt används profilen \"Anonym\", som inte innehåller någonting. Klicka "
"på knappen \"+\" för att skapa en ny profil, och skriv in ett namn på "
"upphovsmannaprofilen."

#: ../../reference_manual/preferences/author_settings.rst:24
msgid "You can then fill out the fields."
msgstr "Därefter kan man fylla i fälten."

#: ../../reference_manual/preferences/author_settings.rst:30
msgid ".. image:: images/preferences/Krita_4_0_preferences_author_page.png"
msgstr ".. image:: images/preferences/Krita_4_0_preferences_author_page.png"

#: ../../reference_manual/preferences/author_settings.rst:30
msgid ""
"The position field is special in that it has a list of hard coded common "
"artists positions it can suggest."
msgstr ""
"Positionsfältet är speciellt på det sätt att det har en lista med vanliga "
"konstnärsroller som det kan föreslå."

#: ../../reference_manual/preferences/author_settings.rst:32
msgid ""
"In older versions of Krita there could only be one of each contact info. In "
"4.0, you can make as many contact entries as you'd like."
msgstr ""
"I äldre versioner av Krita kunde det bara finnas en av varje "
"kontaktinformation. I 4.0 kan man skapa hur många kontaktposter som man vill."

#: ../../reference_manual/preferences/author_settings.rst:34
msgid ""
"Press :guilabel:`Add Contact Info`  to add an entry in the box. By default "
"it will set the type to homepage, because that is the one that causes the "
"least spam. Double |mouseleft| homepage to change the contact type. Double |"
"mouseleft| the \"New Contact Info\" text to turn it into a line edit to "
"change the value."
msgstr ""
"Klicka på :guilabel:`Lägg till kontaktinformation` för att lägga till en "
"post i rutan. Normalt ställer den in typen till hemsida, eftersom det är den "
"som orsakar minst skräppost. Dubbelklicka på hemsidan för att ändra "
"kontakttypen. Dubbelklicka på texten \"Ny kontaktinformation\" för att ändra "
"den till en radeditor för att ändra värdet."

#: ../../reference_manual/preferences/author_settings.rst:37
msgid "Using the new profile"
msgstr "Använda den nya profilen"

#: ../../reference_manual/preferences/author_settings.rst:39
msgid ""
"To use a profile for your current drawing, go to :menuselection:`Settings --"
"> Active Author Profile` and select the name you gave your profile. Then, "
"when pressing :guilabel:`Save` on your current document, you will be able to "
"see your last author profile as the last person who saved it in :"
"menuselection:`File --> Document Information --> Author`."
msgstr ""
"För att använda en profil för den aktuella teckningen, gå till :"
"menuselection:`Inställningar --> Aktivera upphovsmannaprofil` och välja "
"namnet som man gav till sin profil. Därefter, när man klickar på :guilabel:"
"`Spara` för aktuellt dokument, kan man se den senaste upphovsmannaprofil som "
"den senaste personen som sparade det i :menuselection:`Arkiv --> "
"Dokumentinformation --> Upphovsman`."

#: ../../reference_manual/preferences/author_settings.rst:42
msgid "Exporting author metadata to Jpeg and Png"
msgstr "Exportera upphovsmannadata till jpeg och Png"

#: ../../reference_manual/preferences/author_settings.rst:46
msgid ""
"The jpeg and png export both have :guilabel:`Sign with author data` options. "
"Toggling these will store the Nickname and the *first entry in the contact "
"info* into the metadata of png or jpeg."
msgstr ""
"Export till jpeg och png båda har alternativen :guilabel:`Signera med "
"upphovsmannaprofildata`. Att aktivera dem lagrar smeknamnet och *första "
"posten i kontaktinformationen* i metadata för jpeg eller png."

#: ../../reference_manual/preferences/author_settings.rst:48
msgid ""
"For the above example in the screenshot, that would result in: ``ExampleMan "
"(http://example.com)`` being stored in the metadata."
msgstr ""
"För ovanstående exempel på skärmbilden skulle det resultera i att "
"``ExampleMan (http://example.com)`` lagras i metadata."
