# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 18:26+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/dockers/shape_properties_docker.rst:1
msgid "Overview of the shape properties docker."
msgstr "Översikt av panelen Formegenskaper."

#: ../../reference_manual/dockers/shape_properties_docker.rst:15
msgid "Shape Properties Docker"
msgstr "Panelen Formegenskaper"

#: ../../reference_manual/dockers/shape_properties_docker.rst:18
msgid ".. image:: images/dockers/Krita_Shape_Properties_Docker.png"
msgstr ".. image:: images/dockers/Krita_Shape_Properties_Docker.png"

#: ../../reference_manual/dockers/shape_properties_docker.rst:21
msgid ""
"This docker is deprecated, and its functionality is folded into the :ref:"
"`shape_edit_tool`."
msgstr ""
"Användning av den här panelen avråds från, och dess funktionalitet har "
"infogats i :ref:`shape_edit_tool`."

#: ../../reference_manual/dockers/shape_properties_docker.rst:23
msgid ""
"This docker is only functional when selecting a rectangle or circle on a "
"vector layer. It allows you to change minor details, such as the rounding of "
"the corners of a rectangle, or the angle of the formula for the circle-shape."
msgstr ""
"Panelen fungerar bara när en rektangel eller cirkel på ett vektorlager. Den "
"låter dig ändra mindre detaljer, såsom rundningen av hörnen på en rektangel "
"eller vinkel i formeln för cirkelformen."
