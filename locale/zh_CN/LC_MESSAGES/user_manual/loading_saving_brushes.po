msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-30 03:18+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_user_manual___loading_saving_brushes.pot\n"

#: ../../user_manual/loading_saving_brushes.rst:None
msgid ".. image:: images/brushes/Krita_4_0_Brush_Settings_Layout.svg"
msgstr ""

#: ../../user_manual/loading_saving_brushes.rst:0
msgid ".. image:: images/brushes/Krita_4_0_dirty_preset_icon.png"
msgstr ""

#: ../../user_manual/loading_saving_brushes.rst:1
msgid ""
"Detailed guide on the brush settings dialog in Krita as well as how to make "
"your own brushes and how to share them."
msgstr "Krita 笔刷选项面板的详细使用指引，同时介绍如何制作和分享笔刷。"

#: ../../user_manual/loading_saving_brushes.rst:12
msgid "Brush Settings"
msgstr ""

#: ../../user_manual/loading_saving_brushes.rst:17
msgid "Loading and Saving Brushes"
msgstr "笔刷的制作、载入和保存"

#: ../../user_manual/loading_saving_brushes.rst:19
msgid ""
"In the real world, when painting or drawing, you don't just use one tool. "
"You use pencils, erasers, paintbrushes, different types of paint, inks, "
"crayons, etc. All these have different ways of making marks."
msgstr ""
"在现实绘画中，你不仅会运用一种工具作画，你会用到铅笔、橡皮擦、笔刷、蜡笔、不"
"同类型的颜料或者墨水等，每种工具和画材都有它独特的笔迹。"

#: ../../user_manual/loading_saving_brushes.rst:23
msgid ""
"In a digital program like Krita you have something similar. We call this a "
"brush engine. And much like how cars have different engines that give "
"different feels when driving, or how pencils make distinctly different marks "
"than rollerball pens, different brush engines have totally different feels."
msgstr ""
"我们在 Krita 这样的数字绘画程序中也会用到不同的画材，我们开发了不同的笔刷引擎"
"来表现不同的画材。铅笔和圆珠笔的工作原理和使用效果完全不同，所以它们使用的笔"
"刷引擎也是不同的。这就好像不同的汽车要有不同的引擎一样。"

#: ../../user_manual/loading_saving_brushes.rst:29
msgid ""
"The brush engines have a lot of different settings as well. So, you can save "
"those settings into presets."
msgstr "每个笔刷引擎可以有许多不同的选项，你可以把不同的选项组合保存成预设。"

#: ../../user_manual/loading_saving_brushes.rst:32
msgid ""
"Unlike Photoshop, Krita makes a difference between brush-tips and brush-"
"presets. Tips are only a stamp of sorts, while the preset uses a tip and "
"many other settings to create the full brush."
msgstr ""
"和 Photoshop 不同，Krita 的笔尖和笔刷预设并不是一回事。笔尖就像是一个扁平的图"
"章，而笔刷预设则使用笔尖配合其他选项构建出一个完整的笔刷。"

#: ../../user_manual/loading_saving_brushes.rst:37
msgid "The Brush settings drop-down"
msgstr "笔刷选项面板"

#: ../../user_manual/loading_saving_brushes.rst:39
msgid ""
"To start, the Brush Settings Editor panel can be accessed in the toolbar, "
"between the :guilabel:`Blending Modes` button on the right and the :guilabel:"
"`Patterns` button on the left. Alternately, you can use the :kbd:`F5` key to "
"open it."
msgstr ""

#: ../../user_manual/loading_saving_brushes.rst:44
msgid ""
"When you open Brush Settings Editor panel you will see something like this:"
msgstr "打开笔刷选项编辑器面板后你会看到如下界面："

#: ../../user_manual/loading_saving_brushes.rst:48
msgid "Tour of the brush settings drop-down"
msgstr "笔刷选项面板功能分区介绍"

#: ../../user_manual/loading_saving_brushes.rst:53
msgid "The brush settings drop-down is divided into six areas,"
msgstr "笔刷选项下拉面板被分为 6 个区域。"

#: ../../user_manual/loading_saving_brushes.rst:56
msgid "Section A - General Information"
msgstr "A 区 - 一般信息"

#: ../../user_manual/loading_saving_brushes.rst:58
msgid ""
"This contains the **Preset Icon**, **Live Brush Preview**, the **Preset "
"Name**, the **Engine** name, and several buttons for saving, renaming, and "
"reloading."
msgstr ""
"本区显示了 **预设图标**、**笔刷效果预览**、**预设名称**、**引擎类型**，还有保"
"存、重命名和重新载入预设的按钮。"

#: ../../user_manual/loading_saving_brushes.rst:62
msgid ""
"Krita's brush settings are stored into the metadata of a 200x200 png (the "
"KPP file), where the image in the png file becomes the preset icon. This "
"icon is used everywhere in Krita, and is useful for differentiating brushes "
"in ways that the live preview cannot."
msgstr ""
"Krita 的笔刷图标是一张 200x200 的 PNG 图像，它被保存在 KPP 文件的元数据里。"
"Krita 会在界面各处显示笔刷图标，比起笔刷效果预览图，笔刷图标更为简练且易于识"
"别。"

#: ../../user_manual/loading_saving_brushes.rst:67
msgid ""
"The live preview shows a stroke of the current brush as a little s-curve "
"wiggle, with the pressure being non-existent on the left, and increasing to "
"full pressure as it goes to the right. It can thus show the effect of the "
"Pressure, Drawing Angle, Distance, Fade and Fuzzy Dab sensors, but none of "
"the others. For some brush engines it cannot show anything. For the color "
"smudge, filter brush and clone tool, it shows an alternating line pattern "
"because these brush engines use the pixels already on canvas to change their "
"effect."
msgstr ""
"笔刷预览图会显示当前笔刷画出的一条略具 S 形的曲线。它从左到右压力逐渐增大，能"
"够显示压力、笔迹走向、距离、淡化和随机度 (笔尖) 等传感器的效果，但无法显示其"
"他效果。有些笔刷引擎无法正常显示预览。颜色涂抹、滤镜还有克隆引擎的笔刷会显示"
"交替色块的背景图案，因为它们依赖画布上的已有颜色来营造效果。"

#: ../../user_manual/loading_saving_brushes.rst:75
msgid ""
"After the preset name, there's a button for **renaming** the brush. This "
"will save the brush as a new brush and blacklist the previous name."
msgstr ""
"预设名称的右边有一个按钮，用于 **重命名** 当前笔刷。重命名操作会把当前选项保"
"存为一个新笔刷，然后把旧笔刷加入到黑名单。"

#: ../../user_manual/loading_saving_brushes.rst:79
msgid "Engine"
msgstr "引擎"

#: ../../user_manual/loading_saving_brushes.rst:81
msgid ""
"The engine of a brush is the underlying programming that generates the "
"stroke from a brush. What that means is that different brush engines have "
"different options and different results. You can see this as the difference "
"between using crayons, pencils and inks, but because computers are maths "
"devices, most of our brush engines produce different things in a more "
"mathematical way."
msgstr ""
"笔刷引擎是笔刷用来产生笔迹的程序。不同的笔刷引擎具有不同的选项和使用效果，你"
"可以把它们看作是使用不同画材的区别，如蜡笔、铅笔和钢笔等。但从原理上说，笔刷"
"引擎之间的差异更多是在数学上的，因为这毕竟是计算机程序。"

#: ../../user_manual/loading_saving_brushes.rst:88
msgid ""
"For most artists the mathematical nature doesn't matter as much as the "
"different textures and marks each brush engine, and each brush engine has "
"its own distinct flavor and use, and can be further customized by modifying "
"the options."
msgstr ""
"对绝大多数画手而言，笔刷引擎的原理并不重要，只需知道每个引擎可以产生独特的使"
"用效果就够了。笔刷引擎还可以通过配套的选项进行更加深入的调整。"

#: ../../user_manual/loading_saving_brushes.rst:94
msgid "Reloading"
msgstr "重新载入预设"

#: ../../user_manual/loading_saving_brushes.rst:96
msgid ""
"If you change a preset, an icon will appear behind the engine name. This is "
"the :guilabel:`reload` button. You can use it to revert to the original "
"brush settings."
msgstr ""
"如果你更改了某个预设的选项，笔刷引擎名称后面会显示一个图标。这便是 :guilabel:"
"`重新载入` 按钮，你可以用它恢复笔刷的原有选项。"

#: ../../user_manual/loading_saving_brushes.rst:101
msgid "Saving a preset"
msgstr "保存预设"

#: ../../user_manual/loading_saving_brushes.rst:103
msgid ""
"On the right, there's :guilabel:`Save New Brush Preset` and :guilabel:"
"`Overwrite Brush`."
msgstr ""
"在 A 区的右边，你可以找到 :guilabel:`保存为新笔刷` 和 :guilabel:`覆盖原有笔刷"
"` 按钮。"

#: ../../user_manual/loading_saving_brushes.rst:106
msgid ""
"This will only enable if there are any changes. Pressing this will override "
"the current preset with the new settings, keeping the name and the icon "
"intact. It will always make a timestamped back up in the resources folder."
msgstr ""
"此按钮只有在笔刷原有选项发生更改之后才会启用。它会用更改过的选项覆盖当前预设"
"的默认值，保留名称和图标不变。此操作还会在资源文件夹中备份旧预设，并为它的文"
"件名附加时间后缀。"

#: ../../user_manual/loading_saving_brushes.rst:108
msgid "Overwrite Brush"
msgstr "覆盖原有笔刷"

#: ../../user_manual/loading_saving_brushes.rst:111
msgid ""
"Will take the current preset and all its changes and save it as a new "
"preset. If no change was made, you will be making a copy of the current "
"preset."
msgstr ""
"此按钮将把当前预设更改后的选项保存为一个新的预设。如果并未更改选项，则保存为"
"当前预设的副本。"

#: ../../user_manual/loading_saving_brushes.rst:113
msgid "Save New Brush Preset"
msgstr "保存为新笔刷"

#: ../../user_manual/loading_saving_brushes.rst:115
msgid ""
"Save new preset will call up the following window, with a mini scratch pad, "
"and all sorts of options to change the preset icon:"
msgstr ""
"点击“保存为新笔刷”按钮后会弹出下面的对话框，它带有一个迷你绘图区和一些更改预"
"设图标的选项。"

#: ../../user_manual/loading_saving_brushes.rst:119
msgid ".. image:: images/brushes/Krita_4_0_Save_New_Brush_Preset_Dialog.png"
msgstr ""

#: ../../user_manual/loading_saving_brushes.rst:120
msgid ""
"The image on the left is a mini scratchpad, you can draw on it with the "
"current brush, allowing small modifications on the fly."
msgstr ""
"位于该对话框左侧的图像是一个迷你绘图区，你可以用当前笔刷在里面描绘，对已加载"
"的图标进行一些小幅调整。"

#: ../../user_manual/loading_saving_brushes.rst:124
msgid ""
"The Name of your brush. This is also used for the KPP file. If there's "
"already a brush with that name, it will effectively overwrite it."
msgstr ""
"笔刷名称的输入框。也用于 KPP 文件。如果已存在同名笔刷，保存时将覆盖它。"

#: ../../user_manual/loading_saving_brushes.rst:125
msgid "Brush Name"
msgstr "笔刷名称"

#: ../../user_manual/loading_saving_brushes.rst:127
msgid "Load Existing Thumbnail"
msgstr "载入已有缩略图"

#: ../../user_manual/loading_saving_brushes.rst:128
msgid "This will load the existing thumbnail inside the preset."
msgstr "载入预设原有的缩略图。"

#: ../../user_manual/loading_saving_brushes.rst:130
msgid "Load scratch pad thumbnail"
msgstr "载入绘图区缩略图"

#: ../../user_manual/loading_saving_brushes.rst:130
msgid ""
"This will load the dashed area from the big scratch pad (Section C) into the "
"thumbnail area."
msgstr "把笔刷选项面板右侧绘图区 (C 区) 的虚线方框区域载入到缩略图区域。"

#: ../../user_manual/loading_saving_brushes.rst:132
msgid "Load Image"
msgstr "载入图像"

#: ../../user_manual/loading_saving_brushes.rst:133
msgid "With this you can choose an image from disk to load as a thumbnail."
msgstr "从硬盘选取图像载入为缩略图。"

#: ../../user_manual/loading_saving_brushes.rst:134
msgid "Load from Icon Library"
msgstr "从图标库载入"

#: ../../user_manual/loading_saving_brushes.rst:135
msgid "This opens up the icon library."
msgstr "此按钮将打开预设图标库"

#: ../../user_manual/loading_saving_brushes.rst:137
msgid "Clear Thumbnail"
msgstr "清空缩略图"

#: ../../user_manual/loading_saving_brushes.rst:137
msgid "This will make the mini scratch pad white."
msgstr "此按钮将把迷你绘图区清空为白色。"

#: ../../user_manual/loading_saving_brushes.rst:140
msgid "The Icon Library"
msgstr "图标库"

#: ../../user_manual/loading_saving_brushes.rst:142
msgid "To make making presets icons faster, Krita got an icon library."
msgstr "为了让预设图标制作起来更方便，Krita 准备了一个图标库。"

#: ../../user_manual/loading_saving_brushes.rst:145
msgid ".. image:: images/brushes/Krita_4_0_Preset_Icon_Library_Dialog.png"
msgstr ""

#: ../../user_manual/loading_saving_brushes.rst:146
msgid ""
"It allows you to select tool icons, and an optional small emblem. When you "
"press :guilabel:`OK` it will load the resulting combination into the mini "
"scratch pad and you can draw in the stroke."
msgstr ""
"你可以在图标库中选择一个工具图标和一个可选的附加图标。点击 :guilabel:`确定` "
"将把组合后的图标加载到迷你绘图区，你还可以在结果上面使用笔刷进行进一步加工。"

#: ../../user_manual/loading_saving_brushes.rst:150
msgid ""
"If you go to your resources folder, there's a folder there called \"preset"
"\\_icons\", and in this folder there are \"tool\\_icons\" and \"emblem"
"\\_icons\". You can add semi-transparent pngs here and Krita will load those "
"into the icon library as well so you can customize your icons even more!"
msgstr ""
"在 Krita 的资源文件夹里面，你可以找到“preset\\_icons”文件夹，而它又包含"
"了“tool\\_icons”和“emblem\\_icons”两个子文件夹。把带有透明背景的 PNG 图像添加"
"到这两个子文件夹，Krita 就会把它们载入到预设图标库，方便日后选用。"

#: ../../user_manual/loading_saving_brushes.rst:156
msgid ""
"At the top right of the icon library, there are three sliders. They allow "
"you to adjust the tool icon. The top two are the same Hue and Saturation as "
"in HSL adjustment, and the lowest slider is a super simple levels filter. "
"This is done this way because the levels filter allows maintaining the "
"darkest shadows and brightest highlights on a tool icon, making it much "
"better for quick adjustments."
msgstr ""
"在预设图标库对话框的右上角有三个滑动条，你可以用它们调整图标的颜色。色相、饱"
"和度滑动条的作用和 HSL 颜色调整滤镜相同，而亮度滑动条是一个简单的亮度滤镜。此"
"亮度滤镜会保持图标原图中最暗的阴影和最亮的高光，让快速调整更加方便。"

#: ../../user_manual/loading_saving_brushes.rst:163
msgid ""
"If you're done with everything, you can press :guilabel:`Save` in the :"
"guilabel:`Save New Brush Preset` dialog and Krita will save the new brush."
msgstr ""
"完成命名和绘制图标之后，在 :guilabel:`保存为新笔刷预设` 对话框点击 :guilabel:"
"`保存`，Krita 就会把这个新笔刷保存到硬盘。"

#: ../../user_manual/loading_saving_brushes.rst:167
msgid "Section B - The Preset Chooser"
msgstr "B 区 - 预设选单"

#: ../../user_manual/loading_saving_brushes.rst:169
msgid ""
"The preset chooser is much the same as the preset docker and the preset drop-"
"down on the :kbd:`F6` key. It's unique in that it allows you to filter by "
"engine and this is also where you can create brushes for an engine from "
"scratch."
msgstr ""

#: ../../user_manual/loading_saving_brushes.rst:174
msgid ""
"It is by default collapsed, so you will need to press the arrow at the top "
"left of the brush engine to show it."
msgstr ""
"此选单默认是折叠状态，你可以在笔刷选项面板的左上方点击左箭头按钮展开它。"

#: ../../user_manual/loading_saving_brushes.rst:177
msgid ""
"The top drop-down is set to “all” by default, which means it shows all "
"engines. It then shows a tag section where you can select the tags, the "
"preset list and the search bar."
msgstr ""
"笔刷引擎下拉菜单默认为“全部”，意味着显示全部笔刷引擎。在它下面依次显示了标签"
"管理、预设列表和搜索框。"

#: ../../user_manual/loading_saving_brushes.rst:181
msgid ""
"Underneath that there's a plus icon, which when pressed gives you the full "
"list of Krita's engines. Selecting an engine from the list will show the "
"brushes for that engine."
msgstr ""
"再往下可以看到一个 “+” 按钮，点击后会弹出 Krita 的全部笔刷引擎列表。从列表中"
"选取一个引擎，面板中间的 E 区就会加载该引擎的笔刷选项。"

#: ../../user_manual/loading_saving_brushes.rst:185
msgid ""
"The trashcan icon does the same as it does in the preset docker: delete, or "
"rather, blacklist a preset so it won't show up in the list."
msgstr ""
"垃圾桶按钮的作用跟笔刷预设面板里面的一样：删除选中的预设。删除操作不会彻底删"
"除预设，而是把它添加到黑名单，使它不再被显示在列表中。"

#: ../../user_manual/loading_saving_brushes.rst:189
msgid "Section C - The Scratch pad"
msgstr "C 区 - 绘图区"

#: ../../user_manual/loading_saving_brushes.rst:191
msgid ""
"When you tweak your brushes, you want to be able to check what each setting "
"does. That's why, to the right of the settings drop-down, there is a scratch "
"pad."
msgstr ""
"在调整笔刷选项时，你应该需要随时检查它的效果，所以我们特地在笔刷选项下拉面板"
"的右边准备了一个绘图区。"

#: ../../user_manual/loading_saving_brushes.rst:195
msgid ""
"It is by default collapsed, so you will have to press the arrow at the top "
"right of the brush settings to show it."
msgstr ""
"绘图区默认为折叠状态，你可以在笔刷选项面板的右上方点击右箭头按钮展开它。"

#: ../../user_manual/loading_saving_brushes.rst:198
msgid ""
"When saving a new preset, you can choose to get the icon from the scratch "
"pad, this will load the dash area into the mini scratch pad of the Save New "
"Brush Preset dialog."
msgstr ""
"绘图区有一个被虚线圈起的正方形区域，它也叫“缩略图区”。在保存新笔刷预设时可以"
"把缩略图区里面的图像用作预设图标。在“保存为新笔刷”对话框里面，有一个按钮可以"
"把缩略图区的图像加载到它的迷你绘图区。"

#: ../../user_manual/loading_saving_brushes.rst:202
msgid "The scratch pad has four buttons underneath it. These are in order for:"
msgstr "绘图区下面有 4 个按钮，它们依次是："

#: ../../user_manual/loading_saving_brushes.rst:204
msgid "Showing the current brush image"
msgstr "将当前笔刷图像加载到缩略图区"

#: ../../user_manual/loading_saving_brushes.rst:206
msgid "Adding a gradient to the scratch pad (useful for smudge brushes)"
msgstr "添加渐变作为绘图区底色 (对于涂抹笔刷有用)"

#: ../../user_manual/loading_saving_brushes.rst:207
msgid "Filling with the background color"
msgstr "填充背景色"

#: ../../user_manual/loading_saving_brushes.rst:208
msgid "Clearing everything on the scratch pad"
msgstr "清空绘图区画面"

#: ../../user_manual/loading_saving_brushes.rst:211
msgid "Section D - The Options List"
msgstr "D 区 - 选项列表"

#: ../../user_manual/loading_saving_brushes.rst:213
msgid ""
"The options, as stated above, are different per brush engine. These "
"represent the different parameters, toggles and knobs that you can turn to "
"make a brush preset unique. For a couple of options, the main things to "
"change are sliders and check boxes, but for a lot of them, they use curves "
"instead."
msgstr ""
"Krita 的笔刷引擎各具特色，每种引擎的参数、开关、滑动条等选项不尽相同。常见的"
"选项形式为滑动条和复选框，但也有不少选项是通过响应曲线控制的。"

#: ../../user_manual/loading_saving_brushes.rst:219
msgid ""
"Some options can be toggled, as noted by the little check boxes next to "
"them, but others, like flow and opacity are so fundamental to how the brush "
"works, that they are always on."
msgstr ""
"带有复选框的选项可以被打开或者关闭。但流量、不透明度等最基本的笔刷选项不能关"
"闭。"

#: ../../user_manual/loading_saving_brushes.rst:223
msgid ""
"The little padlock icon next to the options is for locking the brush. This "
"has its own page."
msgstr "列表右侧的锁形图标可以锁定对应的笔刷选项。详情在手册的对应页面介绍。"

#: ../../user_manual/loading_saving_brushes.rst:227
msgid "Section E - Option Configuration Widget"
msgstr "E 区 - 选项调整页面"

#: ../../user_manual/loading_saving_brushes.rst:229
msgid ""
"Where section D is the list of options, section E is the widget where you "
"can change things."
msgstr ""
"前面介绍的 D 区是选项的列表，而 E 区则是选项的具体内容，你可以在这里对它们进"
"行修改。"

#: ../../user_manual/loading_saving_brushes.rst:233
msgid "Using sensor curves"
msgstr "使用传感器曲线"

#: ../../user_manual/loading_saving_brushes.rst:235
msgid ""
"One of the big important things that make art unique to the artist who "
"created it is the style of the strokes. Strokes are different because they "
"differ in speed, rotation, direction, and the amount of pressure put onto "
"the stylus. Because these are so important, we would want to customize how "
"these values are understood in detail. The best way to do this is to use "
"curves."
msgstr ""
"每个画手的作品都有自己的风格，这在很大程度上是通过笔画的风格体现的。压感笔的"
"速度、旋转、方向和压力都会影响笔画的效果。这些传感器数据对绘画工作至关重要，"
"为了能够精确地控制软件如何解读它们，最好的办法就是使用响应曲线。"

#: ../../user_manual/loading_saving_brushes.rst:242
msgid ""
"Curves show up with the size widget for example. With an inking brush, we "
"want to have size mapped to pressure. Just toggling the size option in the "
"option list will do that."
msgstr ""
"以笔刷大小选项为例，对于一个勾线笔刷，我们需要把笔刷的大小与压力数据关联起"
"来。要做到这一点，可在选项列表中勾选“大小”选项。"

#: ../../user_manual/loading_saving_brushes.rst:246
msgid ""
"However, different people have different wrists and thus will press "
"differently on their stylus. Someone who presses softly tends to find it "
"easy to make thin strokes, but very difficult to make thick strokes. "
"Conversely, someone who presses hard on their stylus naturally will have a "
"hard time making thin strokes, but easily makes thick ones."
msgstr ""
"每个人的用笔习惯各不相同，所以施加的压力也会存在差异。有些人习惯使用轻柔的笔"
"触，他们画起细线来很拿手，可是要画粗线时就很吃力。而有些人的手劲大的很，他们"
"画细线时难以控制，可画起粗线来却毫不费劲。"

#: ../../user_manual/loading_saving_brushes.rst:252
msgid ""
"Such a situation can be improved by using the curves to map pressure to "
"output thinner lines or thicker ones."
msgstr ""
"为了解决这个问题，我们可以指定一条弯曲的压力响应曲线，使得在同等压力下画出的"
"线条更粗或者更细。"

#: ../../user_manual/loading_saving_brushes.rst:255
msgid ""
"The brush settings curves even have quick curve buttons for these at the "
"top. Someone who has a hard time making small strokes should try the second "
"to last concave button, while someone who has a hard time making thick "
"strokes should try the third button, the S shape."
msgstr ""
"在笔刷选项的曲线图顶部有一系列常用的曲线按钮，只需点击一下即可切换。画细线有"
"困难的人可以使用倒数第二个按钮，它是一条下凹的斜线；而画粗线有困难的可以使用"
"第三个按钮，它是一条 S 形线。"

#: ../../user_manual/loading_saving_brushes.rst:260
msgid "Underneath the curve widget there are two more options:"
msgstr "在曲线图下方还有两个选项："

#: ../../user_manual/loading_saving_brushes.rst:263
msgid ""
"This is for the list of sensors. Toggling this will make all the sensors use "
"the same curve. Unchecked, all checked sensors will have separate curves."
msgstr ""
"此选项影响列表中的各个传感器。勾选此选项，则所有传感器将使用相同的曲线。取消"
"勾选，则每个传感器会使用各自独立的曲线。"

#: ../../user_manual/loading_saving_brushes.rst:264
msgid "Share Curves across all settings"
msgstr "所有传感器统一曲线"

#: ../../user_manual/loading_saving_brushes.rst:267
msgid ""
"This indicates how the multiple values of the sensor curves are used. The "
"curves always go from 0 to 1.0, so if one curve outputs 0.5 and the other "
"0.7, then..."
msgstr ""
"此选项控制不同传感器曲线的协同效果。曲线的输出数值在 0 到 1.0 的区间上，如果"
"一条曲线的输出是 0.5，另外一条曲线的输出是 0.7，那么……"

#: ../../user_manual/loading_saving_brushes.rst:271
msgid "Multiply"
msgstr "相乘"

#: ../../user_manual/loading_saving_brushes.rst:272
msgid "Will multiply the two values, 0.5\\*0.7 = 0.35."
msgstr "把两者数值相乘，0.5 x 0.7 = 0.35。"

#: ../../user_manual/loading_saving_brushes.rst:274
msgid "Addition"
msgstr "相加"

#: ../../user_manual/loading_saving_brushes.rst:274
msgid ""
"Will add the two to a maximum of 1.0, so 0.5+0.7 = 1.2, which is then capped "
"at 1.0."
msgstr "把两者数值相加，最大值 1.0。因为 0.5 + 0.7 = 1.2，所以截为 1.0。"

#: ../../user_manual/loading_saving_brushes.rst:277
msgid "Maximum"
msgstr "最大值"

#: ../../user_manual/loading_saving_brushes.rst:277
msgid ""
"Will compare the two and pick the largest. So in the case of 0.5 and 0.7, "
"the result is 0.7."
msgstr ""
"比较两者数值，取其中最大值。此例子中数值为 0.5 和 0.7，因此结果为 0.7。"

#: ../../user_manual/loading_saving_brushes.rst:280
msgid "Minimum"
msgstr "最小值"

#: ../../user_manual/loading_saving_brushes.rst:280
msgid ""
"Will compare the two and pick the smallest. So in the case of 0.5 and 0.7, "
"the result is 0.5."
msgstr ""
"比较两者数值，取其中最小值。此例子中数值为 0.5 和 0.7，因此结果为 0.5。"

#: ../../user_manual/loading_saving_brushes.rst:283
msgid "Will subtract the smallest value from the largest, so 0.7-0.5 = 0.2."
msgstr "用两者数值的最大值减去最小值，因此 0.7 - 0.5 = 0.2。"

#: ../../user_manual/loading_saving_brushes.rst:284
msgid "Curves Calculation Mode"
msgstr "曲线计算方式"

#: ../../user_manual/loading_saving_brushes.rst:284
msgid "Difference"
msgstr "差值"

#: ../../user_manual/loading_saving_brushes.rst:286
msgid "It's maybe better to see with the following example:"
msgstr "下面我们用更直观的方式进行说明："

#: ../../user_manual/loading_saving_brushes.rst:289
msgid ".. image:: images/brushes/Krita_4_0_brush_curve_calculation_mode.png"
msgstr ""

#: ../../user_manual/loading_saving_brushes.rst:290
msgid ""
"The first two are regular, the rest with different multiplication types."
msgstr "第 1、2 组是常规曲线，其余的则应用了不同的计算模式。"

#: ../../user_manual/loading_saving_brushes.rst:292
msgid "Is a brush with size set to the distance sensor."
msgstr "笔刷大小与距离传感器关联。"

#: ../../user_manual/loading_saving_brushes.rst:293
msgid "Is a brush with the size set to the fade sensor."
msgstr "笔刷大小与淡化传感器相关联。"

#: ../../user_manual/loading_saving_brushes.rst:294
msgid "The size is calculated from the fade and distance sensors multiplied."
msgstr "笔刷大小与淡化、距离传感器数值的相乘结果相关联。"

#: ../../user_manual/loading_saving_brushes.rst:295
msgid ""
"The size is calculated from the fade and distance sensors added to each "
"other. Notice how thick it is."
msgstr "笔刷大小与淡化、距离传感器数值的相加结果相关联。线条明显更粗。"

#: ../../user_manual/loading_saving_brushes.rst:297
msgid ""
"The size takes the maximum value from the values of the fade and distance "
"sensors."
msgstr "笔刷大小与淡化、距离传感器数值里面的最大值相关联。"

#: ../../user_manual/loading_saving_brushes.rst:299
msgid ""
"The size takes the minimum value from the values of the fade and distance "
"sensors."
msgstr "笔刷大小与淡化、距离传感器数值里面的最小值相关联。"

#: ../../user_manual/loading_saving_brushes.rst:301
msgid ""
"The size is calculated by having the largest of the values subtracted with "
"the smallest of the values."
msgstr "笔刷大小与淡入、距离传感器数值两者的差值相关联。"

#: ../../user_manual/loading_saving_brushes.rst:305
msgid "Section F - Miscellaneous options"
msgstr "F 区 - 其他选项"

#: ../../user_manual/loading_saving_brushes.rst:308
msgid ""
"This enables dirty presets. Dirty presets store the tweaks you make as long "
"as this session of Krita is active. After that, they revert to default. "
"Dirtied presets can be recognized by the icon in the top-left of the preset."
msgstr ""
"勾选此选项后，Krita 会暂时保留对预设选项的修改，直至当前会话结束。重新启动 "
"Krita 之后，所有选项将被恢复为预设的默认值。如果某个预设选项已被改动，它在笔"
"刷预设面板中的图标左上会显示一个角标。"

#: ../../user_manual/loading_saving_brushes.rst:316
msgid "Temporarily Save Tweaks to Preset (Dirty Presets)"
msgstr "临时保存选项修改"

#: ../../user_manual/loading_saving_brushes.rst:316
msgid ""
"The icon in the top left of the first two presets indicate it is “Dirty”, "
"meaning there are tweaks made to the preset."
msgstr ""
"中间的两个预设在它们图标的左上角显示了“已修改”角标，表明它们的选项已被修改。"

#: ../../user_manual/loading_saving_brushes.rst:319
msgid "Eraser Switch Size"
msgstr "擦除模式独立大小"

#: ../../user_manual/loading_saving_brushes.rst:319
msgid ""
"This switches the brush to a separately stored size when using the :kbd:`E` "
"key."
msgstr "勾选此选项后，按 :kbd:`E` 切换的擦除模式将独立保存笔刷大小。"

#: ../../user_manual/loading_saving_brushes.rst:321
msgid "Eraser Switch Opacity"
msgstr "擦除模式独立不透明度"

#: ../../user_manual/loading_saving_brushes.rst:322
msgid "Same as above, but then with Eraser opacity."
msgstr "和上面一样，但控制的是擦除模式的不透明度。"

#: ../../user_manual/loading_saving_brushes.rst:324
msgid ""
"This allows you to toggle instant preview on the brush. The Instant Preview "
"has a super-secret feature: when you press the instant preview label, and "
"then right click it, it will show a threshold slider. This slider determines "
"at what brush size instant preview is activated for the brush. This is "
"useful because small brushes can be slower with instant preview, so the "
"threshold ensures it only activates when necessary."
msgstr ""
"勾选此选项将启用笔刷的即时预览功能。在“即时预览”文字上右键单击可以调出该功能"
"的阈值滑动条，它控制即时预览功能生效的最小笔刷大小。这是因为即时预览模式会造"
"成延迟，所以我们要确保此功能只在需要时被触发。"

#: ../../user_manual/loading_saving_brushes.rst:330
msgid "Instant Preview"
msgstr "即时预览"

#: ../../user_manual/loading_saving_brushes.rst:333
msgid "The On-canvas brush settings"
msgstr "在画布上修改笔刷选项"

#: ../../user_manual/loading_saving_brushes.rst:335
msgid ""
"There are on-canvas brush settings. If you open up the pop-up palette, there "
"should be an icon on the bottom-right. Press that to show the on-canvas "
"brush settings. You will see several sliders here, to quickly make small "
"changes."
msgstr ""
"在画布上右键单击打开浮动画具板，点开它右下角的右箭头按钮，它会显示一些主要选"
"项的滑动条。它们可以让你随手在画布上修改笔刷选项。"

#: ../../user_manual/loading_saving_brushes.rst:340
msgid ""
"At the top it shows the currently active preset. Next to that is a settings "
"button, click that to get a list of settings that can be shown and organized "
"for the given brush engine. You can use the up and down arrows to order "
"their position, and then left and right arrows to add or remove from the "
"list. You can also drag and drop."
msgstr ""
"该面板的顶部显示了当前笔刷预设的名称。单击右上角的按钮将显示能够添加到此面板"
"的滑动条列表。你可以用该对话框的上下左右按钮移动项目，也可以直接拖放它们。"

#: ../../user_manual/loading_saving_brushes.rst:347
msgid "Making a Brush Preset"
msgstr "制作笔刷预设"

#: ../../user_manual/loading_saving_brushes.rst:349
msgid "Now, let's make a simple brush to test the waters with:"
msgstr "现在是时候制作一个简单的笔刷以检验我们的学习成果了："

#: ../../user_manual/loading_saving_brushes.rst:352
msgid "Getting a default for the brush engine."
msgstr "获取笔刷引擎默认选项"

#: ../../user_manual/loading_saving_brushes.rst:354
msgid "First, open the settings with the :kbd:`F5` key."
msgstr ""

#: ../../user_manual/loading_saving_brushes.rst:356
msgid ""
"Then, press the arrow on the upper left to open the preset chooser. There, "
"press the “+” icon to get a list of engines. For this brush we're gonna make "
"a pixel brush."
msgstr ""
"点击面板左上的左箭头按钮，展开笔刷预设选单。在选择器的左下角单击 “+” 按钮，打"
"开可用引擎列表。在这个例子里我们将制作一个像素引擎的笔刷。"

#: ../../user_manual/loading_saving_brushes.rst:361
msgid "Example: Making an inking brush"
msgstr "勾线笔刷制作范例"

#: ../../user_manual/loading_saving_brushes.rst:363
msgid ""
"Draw on the scratch pad to see what the current brush looks like. If done "
"correctly, you should have a 5px wide brush that has pressure set to opacity."
msgstr ""
"在笔刷选项面板右侧的绘图区感受一下当前笔刷的效果。如果一切正常，笔刷的直径应"
"该为 5px，压感笔的压力控制笔刷的不透明度。"

#: ../../user_manual/loading_saving_brushes.rst:366
msgid ""
"Let us turn off the opacity first. Click on the :ref:`opacity "
"<option_opacity_n_flow>` option in the right-hand list. The settings should "
"now be changed to a big curve. This is the sensor curve."
msgstr ""
"让我们先把不透明度选项关闭。在笔刷预设选择器右边的列表选中 :ref:`不透明度 "
"<option_opacity_n_flow>` 选项，你会发现面板中间的选项变成了曲线图，这是压感笔"
"传感器的响应曲线。"

#: ../../user_manual/loading_saving_brushes.rst:370
msgid "Uncheck the :guilabel:`Enable Pen Settings` checkbox."
msgstr "取消勾选 :guilabel:`启用压感笔传感器选项`。"

#: ../../user_manual/loading_saving_brushes.rst:371
msgid ""
"Test on the scratch pad... there still seems to be something affecting "
"opacity. This is due to the :ref:`flow <option_opacity_n_flow>` option."
msgstr ""
"在绘图区试用一下，你会发现还是有一些东西在影响不透明度，这是 :ref:`流量 "
"<option_opacity_n_flow>` 选项在作怪。"

#: ../../user_manual/loading_saving_brushes.rst:375
msgid ""
"Select the Flow option from the list on the right hand. Flow is like "
"Opacity, except that Flow is per dab, and opacity is per stroke."
msgstr ""
"从刚才的列表中选中流量选项。流量的作用和不透明度相似，但也有本质的不同。流量"
"控制一条笔画中每次笔尖印迹的透明度，而不透明度控制一条完整笔画的透明度。"

#: ../../user_manual/loading_saving_brushes.rst:377
msgid ""
"Uncheck the :guilabel:`Enable Pen Settings` checkbox here as well. Test "
"again."
msgstr ""
"取消勾选流量选项中的 :guilabel:`启用压感笔传感器选项`。现在再到绘图区试试效"
"果。"

#: ../../user_manual/loading_saving_brushes.rst:378
msgid ""
"Now you should be getting somewhere towards an inking brush. It is still too "
"small however, and kinda grainy looking. Click :ref:`Brush Tip "
"<option_brush_tip>` in the brush engine options."
msgstr ""
"现在这个笔刷开始有点勾线笔的样子了。不过它还是太细，而且边缘也显得太硬。要改"
"善这些不足，单击笔刷引擎选项列表中的 :ref:`笔尖 <option_brush_tip>` 选项。"

#: ../../user_manual/loading_saving_brushes.rst:381
msgid ""
"Here, the diameter is the size of the brush-tip. You can touch the slider "
"change the size, or right-click it and type in a value. Set it to 25 and "
"test again. It should be much better."
msgstr ""
"在这个选项页面里，直径选项控制笔尖大小。你可以拖动滑动条调整笔刷大小，也可以"
"右键单击滑动条后输入一个数值。把直径设为 25，然后再到绘图区试试看。现在效果应"
"该大为改观。"

#: ../../user_manual/loading_saving_brushes.rst:384
msgid ""
"Now to make the brush feel a bit softer, turn down the fade parameter to "
"about 0.9. This'll give the *brush mask* a softer edge."
msgstr ""
"现在我们要把笔刷边缘变得柔和一些。把“淡化”参数设为 0.9 左右。这样会让笔刷蒙版"
"生成一个更加柔和的边缘。"

#: ../../user_manual/loading_saving_brushes.rst:386
msgid ""
"If you test again, you'll notice the fade doesn't seem to have much effect. "
"This has to do with the spacing of the dabs: The closer they are together, "
"the harder the line is. By default, this is 0.1, which is a bit low. If you "
"set it to 10 and test, you'll see what kind of effect spacing has. The :ref:"
"`Auto <option_spacing>` checkbox changes the way the spacing is calculated, "
"and Auto Spacing with a value of 0.8 is the best value for inking brushes. "
"Don't forget that you can use right-click to type in a value."
msgstr ""
"然而这次修改后效果却不太明显。这是因为笔尖印迹的间距太近造成的：笔尖印迹间距"
"越小，笔尖印迹就会相互重叠，造成硬边效果。笔尖间距默认为 0.1，这是比较小的。"
"如果你把它设为 10，就会明白其中奥妙了。勾选间距参数的 :ref:`自动 "
"<option_spacing>` 复选框，将间距设为 0.8，这就是勾线笔刷的最佳数值。记得右键"
"单击可以直接输入数字。"

#: ../../user_manual/loading_saving_brushes.rst:395
msgid ""
"Now, when you test, the fade seems to have a normal effect... except on the "
"really small sizes, which look pixelly. To get rid of that, check the anti-"
"aliasing check box. If you test again, the lines should be much nicer now."
msgstr ""
"现在线条的边缘看起来已经软硬适中了，只是笔刷尺寸被调小之后线条会有些锯齿。我"
"们可以勾选“抗锯齿”复选框，这样就可以画出令人满意的线条了。"

#: ../../user_manual/loading_saving_brushes.rst:401
msgid "Saving the new Brush"
msgstr "保存制作的笔刷"

#: ../../user_manual/loading_saving_brushes.rst:403
msgid ""
"When you're satisfied, go to the upper left and select :guilabel:`Save New "
"Brush Preset`."
msgstr ""
"如果你对结果感到满意，可到笔刷选项面板右上角，单击 :guilabel:`保存为新笔刷` "
"按钮。"

#: ../../user_manual/loading_saving_brushes.rst:406
msgid ""
"You will get the save preset dialog. Name the brush something like “My "
"Preset”. Then, select :guilabel:`Load from Icon Library` to get the icon "
"library. Choose a nice tool icon and press :guilabel:`OK`."
msgstr ""
"这将打开“保存新笔刷预设”对话框。将笔刷名称改为“你想要的笔刷名称”。然后单击 :"
"guilabel:`从图标库载入缩略图`，打开预设图标库。选择一个好看的工具图标，然后单"
"击 :guilabel:`确定`。"

#: ../../user_manual/loading_saving_brushes.rst:410
msgid ""
"The icon will be loaded into the mini scratch pad on the left. Now doodle a "
"nice stroke next to it. If you feel you messed up, just go back to the icon "
"library to load a new icon."
msgstr ""
"你刚才选取的图标将会被载入到左边的迷你绘图区。现在你可以直接在迷你绘图区作"
"画，例如在工具图标的笔尖下面画出一个示意的笔画。如果不小心给画坏了，就重新打"
"开图标库载入一个新图标，重新尝试。"

#: ../../user_manual/loading_saving_brushes.rst:414
msgid "Finally press :guilabel:`Save`, and your brush should be done."
msgstr "最后，单击 :guilabel:`保存` 按钮，你的新笔刷预设就大功告成了！"

#: ../../user_manual/loading_saving_brushes.rst:416
msgid "You can further modify your inking brush by..."
msgstr "你还可以进一步调整这个勾线笔刷："

#: ../../user_manual/loading_saving_brushes.rst:419
msgid ""
"Changing the amount of pressure you need to put on a brush to make it full "
"size."
msgstr "调整画出最大直径所需压力"

#: ../../user_manual/loading_saving_brushes.rst:419
msgid ""
"To do this, select the :ref:`size <option_size>` option, and press the "
"pressure sensor from the list next to the curve. The curve should look like "
"a straight line. Now if you want a brush that gets big with little pressure, "
"tick on the curve to make a point, and drag the point to the upper-left. The "
"more the point is to the upper-left, the more extreme the effect. If you "
"want instead a brush that you have to press really hard on to get to full "
"size, drag the dot to the lower-right. Such a brush is useful for fine "
"details. Don't forget to save the changes to your brush when done."
msgstr ""
"在笔刷引擎选项列表里选中 :ref:`大小 <option_size>` ，确保已经在曲线图左边的传"
"感器列表里勾选了“压力”选项。如果你想要施加很小的压力就可以画出全宽度的线条，"
"请先在曲线上点击一下，这样可以创建一个节点，把这个节点拉到曲线图偏左上的位"
"置。节点越靠近左上角，效果就越明显。如果你想要施加再大的压力也不会画得太粗，"
"就反过来把节点向右下角拉。这样的笔刷适合勾画细节。调整结束之后，别忘了保存或"
"者覆盖你的笔刷预设。"

#: ../../user_manual/loading_saving_brushes.rst:422
msgid "Making the fine lines look even softer by using the flow option."
msgstr "用流量选项让细线变得更为柔和"

#: ../../user_manual/loading_saving_brushes.rst:422
msgid ""
"To do this, select the flow option, and turn back on the :guilabel:`Enable "
"Pen Settings` check box. Now if you test this, it is indeed a bit softer, "
"but maybe a bit too much. Click on the curve to make a dot, and drag that "
"dot to the top-left, half-way the horizontal of the first square of the "
"grid. Now, if you test, the thin lines are much softer, but the hard your "
"press, the harder the brush becomes."
msgstr ""
"在笔刷引擎选项列表里选中 :ref:`流量 <option_size>` ，把刚才取消的 :guilabel:`"
"启用压感笔选项` 重新勾选。你会发现笔画现在又貌似变得太柔和了。在曲线上点击以"
"创建一个节点，把这个节点拉到曲线图左上角网格的底边中间点附近。现在当你用轻柔"
"的压力描绘时，线条会更加柔和，而当你加大压力时，线条边缘又会变得硬朗起来。"

#: ../../user_manual/loading_saving_brushes.rst:425
msgid "Sharing Brushes"
msgstr "分享笔刷预设"

#: ../../user_manual/loading_saving_brushes.rst:427
msgid ""
"Okay, so you've made a new brush and want to share it. There are several "
"ways to share a brush preset."
msgstr ""
"好了，你现在终于制作了一个笔刷。如果你想要分享它，你可以通过下面几种方式来分"
"享笔刷预设。"

#: ../../user_manual/loading_saving_brushes.rst:430
msgid ""
"The recommended way to share brushes and presets is by using the resource "
"bundle system. We have detailed instructions on how to use them on the :ref:"
"`resource management page <resource_management>`."
msgstr ""
"我们推荐你使用 Krita 的资源包系统来分享笔刷以及其他预设。我们在 :ref:`资源管"
"理页面 <resource_management>` 对此进行了详细介绍。"

#: ../../user_manual/loading_saving_brushes.rst:434
msgid ""
"However, there are various old-fashioned ways of sharing brushes that can be "
"useful when importing and loading very old packs:"
msgstr ""
"不过我们也可以使用一些比较老派的方式来分享笔刷，它们在导入或者载入比较老旧的"
"资源包时说不定还能派上用场："

#: ../../user_manual/loading_saving_brushes.rst:438
msgid "Sharing a single preset"
msgstr "分享单个预设"

#: ../../user_manual/loading_saving_brushes.rst:440
msgid "There are three types of resources a single preset can take:"
msgstr "单个预设文件可以保存三种资源："

#: ../../user_manual/loading_saving_brushes.rst:442
msgid ""
"A Paintoppreset file: This is the preset proper, with the icon and the "
"curves stored inside."
msgstr "单个 Paintoppreset 文件：这是一个完整的预设文件，内含图标和响应曲线。"

#: ../../user_manual/loading_saving_brushes.rst:444
msgid ""
"A Brush file: This is the brush tip. When using masked brushes, there are "
"two of these."
msgstr ""

#: ../../user_manual/loading_saving_brushes.rst:446
msgid "A Pattern file: this is when you are using textures."
msgstr "单个图案文件：在使用纹理时会用得到。"

#: ../../user_manual/loading_saving_brushes.rst:448
msgid ""
"So when you have a brush that uses unique predefined tips for either brush "
"tip or masked brush, or unique textures you will need to share those "
"resources as well with the other person."
msgstr ""
"如果你的笔刷的笔尖或者蒙版笔刷使用了独特的预制笔尖形状，或者用到了材质时，你"
"必须把这些相关资源也分享给对方。"

#: ../../user_manual/loading_saving_brushes.rst:452
msgid ""
"To find those resources, go to :menuselection:`Settings --> Manage Resources "
"--> Open Resource Folder`."
msgstr ""
"要找到这些资源，可点击菜单栏的 :menuselection:`设置 --> 管理资源 --> 打开资源"
"文件夹`。"

#: ../../user_manual/loading_saving_brushes.rst:454
msgid ""
"There, the preset file will be inside paintoppresets, the brush tips inside "
"brushes and the texture inside patterns."
msgstr ""
"在这个文件夹里，预设文件被保存在 paintoppresets 子文件夹，笔尖被保存在 "
"brushes 子文件夹，材质被保存在 patterns 子文件夹。"

#: ../../user_manual/loading_saving_brushes.rst:458
msgid "Importing a single KPP file."
msgstr "导入单个 KPP 文件。"

#: ../../user_manual/loading_saving_brushes.rst:460
msgid ""
"Now, if you want to use the single preset, you should go to the preset "
"chooser on the :kbd:`F6` key and press the folder icon there. This will give "
"a file dialog. Navigate to the kpp file and open it to import it."
msgstr ""

#: ../../user_manual/loading_saving_brushes.rst:464
msgid ""
"If there are brush tips and patterns coming with the file, do the same with "
"pattern via the pattern docker, and for the brush-tip go to the settings "
"drop-down (:kbd:`F5`) and then go to the “brush-tip” option. There, select "
"predefined brush, and then the “import” button to call up the file dialog."
msgstr ""
"如果该预设文件需要配套的笔尖和图案，你还要分别导入它们。要导入图案，请使用图"
"案工具面板。要导入笔尖，请在笔刷选项面板 (:kbd:`F5`) 的“笔尖”选项页面选中“预"
"制形状”，然后点击它的“导入”按钮。"

#: ../../user_manual/loading_saving_brushes.rst:471
msgid "Sharing via ZIP (old-fashioned)"
msgstr "旧式 ZIP 预设文件包"

#: ../../user_manual/loading_saving_brushes.rst:473
msgid ""
"Sharing via ZIP should be replaced with resource bundles, but older brush "
"packs are stored in zip files."
msgstr ""
"其实旧式的 ZIP 预设文件包早就应该被新版资源包给取代掉了，不过的确也有一些比较"
"老旧的笔刷包是用 ZIP 文件保存的。"

#: ../../user_manual/loading_saving_brushes.rst:477
msgid "Using a ZIP with the relevant files."
msgstr "如何使用 ZIP 打包的资源文件"

#: ../../user_manual/loading_saving_brushes.rst:479
msgid ""
"Go to :menuselection:`Settings --> Manage Resources --> Open Resource "
"Folder` to open the resource folder."
msgstr "点击菜单栏的 :menuselection:`设置 --> 管理资源 --> 打开资源文件夹`。"

#: ../../user_manual/loading_saving_brushes.rst:480
msgid "Then, open up the zip file."
msgstr "打开你的 ZIP 压缩包。"

#: ../../user_manual/loading_saving_brushes.rst:481
msgid ""
"Copy the brushes, paintoppresets and patterns folders from the zip file to "
"the resource folder. You should get a prompt to merge the folders, agree to "
"this."
msgstr ""
"把压缩包里面的文件夹解压出来，覆盖资源文件夹里面的同名文件夹，如果提示是否合"
"并，请同意。"

#: ../../user_manual/loading_saving_brushes.rst:484
msgid "Restart Krita."
msgstr "重新启动 Krita。"

#: ../../user_manual/loading_saving_brushes.rst:485
msgid "Enjoy your brushes!"
msgstr "尽情使用新笔刷吧！"
